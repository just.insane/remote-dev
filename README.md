# Configuration for remove dev machine

This ansible playbook configures a remote dev machine with tools and code-server in order to code anywhere that has a web browser and an internet connection.

## TO-DO

- [X] Create SSH and GPG keys and upload them to GitHub/GitLab/DigitalOcean
- [ ] Install wireguard server and provide client info for laptop/phone
- [ ] Configure site-to-site VPN for administering on-prem resources
  - [ ] Alternatively, run remote-dev on prem
- [ ] Investigate re-writing https://github.com/devonlineco/ansible-roles